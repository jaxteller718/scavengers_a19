Key,Source,Context,English

meleeToolSalvageT4Crowbar,items,Melee,Crowbar,,,,,
meleeToolSalvageT4CrowbarDesc,items,Melee,"Not only is this crowbar good for smashing the heads of the undead, it can also be used to harvest more quickly.",,,,,
meleeToolSalvageT4CrowbarSchematic,items,Melee,Crowbar Schematic,,,,,
